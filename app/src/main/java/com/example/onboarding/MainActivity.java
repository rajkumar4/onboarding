package com.example.onboarding;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ViewPager slideViewPager;
    LinearLayout indicator_layout;
    Button backBtn,nextBtn,skipBtn;

    TextView[] dots;
    ViewPagerAdapter viewPagerAdapter;

    private PrefManager prefManager;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backBtn=findViewById(R.id.backBtn);
        nextBtn=findViewById(R.id.nextBtn);
        skipBtn=findViewById(R.id.skipButton);
        slideViewPager=findViewById(R.id.slideViewPager);
        indicator_layout=findViewById(R.id.indicator_layout);

        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getItem(0)>0){
                    slideViewPager.setCurrentItem(getItem(-1),true);
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getItem(0)<3){
                    slideViewPager.setCurrentItem(getItem(1),true);
                }
                else {
                    Intent i =  new Intent(MainActivity.this,SecondActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(MainActivity.this,SecondActivity.class);
                startActivity(i);
                finish();
            }
        });

        viewPagerAdapter =  new ViewPagerAdapter(this);
        slideViewPager.setAdapter(viewPagerAdapter);
        setUpIndicator(0);
        slideViewPager.addOnPageChangeListener(viewListener);
    }
    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(MainActivity.this, SecondActivity.class));
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setUpIndicator(int position){
        dots=new  TextView[4];
        indicator_layout.removeAllViews();
        for (int i =0;i<dots.length;i++){
            dots[i]=new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.inactive,getApplicationContext().getTheme()));
            indicator_layout.addView(dots[i]);
        }
        dots[position].setTextColor(getResources().getColor(R.color.active,getApplicationContext().getTheme()));
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onPageSelected(int position) {
            setUpIndicator(position);
            if(position>0){
                backBtn.setVisibility(View.VISIBLE);
            }
            else{
                backBtn.setVisibility(View.INVISIBLE);
            }
        }
        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };
    private int getItem(int i){
        return slideViewPager.getCurrentItem()+i;
    }
}